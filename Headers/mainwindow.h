﻿/*********************************************************************
 Copyright 2017 Yaroslav Belykh (aka Yamah)

 This file is part of MakeUniversalBootFlash for x86 and amd64 PC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QProcess>
#include <QAction>
#include <QMenu>
#include <QMessageBox>
#include <QMap>
#include <QVariant>
#include <QLabel>
#include <QTimer>
#include <QDir>
#include <QFile>
#include <QDebug>
#include <QtCore/qmath.h>
#include <QTextDocumentWriter>

typedef struct vDiskInfo
{
    QString dSize;
    int dSizeBytes;
    int dSizeBlocks;
}TDiskInfo;

typedef struct vDiskStruct
{
    QByteArray pNumb;
    QByteArray pSize;
    QByteArray pType;
    QString sizeName;
    QString typeName;
}TDiskStruct;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void proccessFinished(int, QProcess::ExitStatus);
    void haseTimeOut();
    void haseReadError();
    void haseReadStandart();

    void OpenAboutQt();
    void doExit();

    void on_pbClear_clicked();

    void on_pbrePlace_clicked();

    void on_pbRefreshList_clicked();

    void on_comboBox_currentTextChanged(const QString &arg1);

    void on_pbLegacy_clicked();

    void on_pbEFI_clicked();

    void on_pbKill_clicked();

    void on_pbClearFast_clicked();

    void on_checkBox_toggled(bool checked);

    void on_pbEFIx32_clicked();

private:
    Ui::MainWindow *ui;

    QLabel *PidLabel;
    QLabel *PidValueLabel;

    QProcess *mainProcess;
    QTimer *mainTimer;

    QMap<QString, TDiskInfo> DiskInfo;
    QMap<int, TDiskStruct> DiskStruct;

    int procID;
    int gdiskCMD;
    int curPartition;

    int blockCount;

    int maxCurPartition;

    void getFlashList();

    inline bool notMounted(QString);
    inline bool isRemovable(QString);

    inline void isWinComp();

    inline void createProcess(int curID);
    inline void clearProcess(void);

    inline void disableButtons(void);

    inline QString getSharedPath(void);

    inline bool isMountPartition(QString inpPart, QString mountPoint);

    QString mountPath(int diskID, bool mp = true, QString MountPath = "");
    QString readResConfFile(QString);

    QString getUUID(QString devName);

    int pidIntl;

    QString *iArch;

    inline void createEfiConfig(void);
    inline void createGrubConfig(void);
    inline void extractEFIfile(void);
    inline void extractGrubFolder(void);
};

#endif // MAINWINDOW_H
