/*********************************************************************
 Copyright 2017 Yaroslav Belykh (aka Yamah)

 This file is part of MakeUniversalBootFlash for x86 and amd64 PC

 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*********************************************************************/

#include "Headers/mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    pidIntl=0;
    procID=0;
    PidLabel = new QLabel(statusBar());
    PidLabel->setObjectName(QStringLiteral("PID"));
    PidLabel->setText("PID: ");
    statusBar()->addWidget(PidLabel);
    PidValueLabel = new QLabel(statusBar());
    PidValueLabel->setObjectName(QStringLiteral("PID"));
    PidValueLabel->setText("");
    statusBar()->addWidget(PidValueLabel);
    disableButtons();

    connect(ui->actionAbout_Qt, SIGNAL(triggered()), this, SLOT(OpenAboutQt()));
    connect(ui->actionExit, SIGNAL(triggered()), this, SLOT(doExit()));

    ui->actionAbout->setVisible(false);

    on_pbRefreshList_clicked();
}

MainWindow::~MainWindow()
{
    if (procID>0)
    {
        clearProcess();
        if (procID==1)
        {
            disconnect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
            delete mainTimer;
            mainTimer=0;
        }
    }
    delete ui;
}

void MainWindow::proccessFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    QString outputStr;
    PidValueLabel->clear();
    pidIntl=0;
    ui->pbKill->setEnabled(false);
    outputStr=QVariant(mainProcess->readAllStandardOutput()).toString();
    if (!outputStr.isEmpty())
        {outputStr.append("\n");}
    ui->textEdit->append(outputStr);
    if (exitCode!=0)
    {
        outputStr.clear();
        outputStr=QVariant(mainProcess->readAllStandardError()).toString();
        if (!outputStr.isEmpty())
            {outputStr.append("\n");}
        ui->textEdit->append(outputStr);
    }
    switch (procID)
    {
        case 1:
        {
            mainTimer->stop();
            clearProcess();
            disconnect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
            ui->progressBar->setEnabled(false);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            delete mainTimer;
            mainTimer=0;
        }
        break;
        case 2:
        {
            mainTimer->stop();
            ui->textEdit->append("dd if=/dev/zero of="+ui->comboBox->currentText()+" seek="+(QVariant(DiskInfo[ui->comboBox->currentText()].dSizeBlocks-2048)).toString()+"\n");
            mainProcess->start("dd", QStringList() << "if=/dev/zero" << "of="+ui->comboBox->currentText() << "seek="+(QVariant(DiskInfo[ui->comboBox->currentText()].dSizeBlocks-2048)).toString());
            mainProcess->waitForStarted();
            pidIntl=mainProcess->pid();
            mainTimer->start();
            PidValueLabel->setText(QVariant(pidIntl).toString());
            procID++;
            ui->pbKill->setEnabled(true);
        }
        break;
        case 3:
        {
            mainTimer->stop();
            clearProcess();
            disconnect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
            ui->progressBar->setEnabled(false);
            ui->progressBar->setVisible(false);
            ui->progressBar->setValue(0);
            delete mainTimer;
            mainTimer=0;
        }
        break;
        case 4:
        {
            if (exitStatus==QProcess::NormalExit)
            {
                if (!ui->chbFAT32->isChecked())
                {
                    ui->textEdit->append("mkfs.vfat -n EFI_BOOT " + ui->comboBox->currentText()+QVariant(DiskStruct[2].pNumb).toString()+"\n");
                    mainProcess->start("mkfs.vfat", QStringList() << "-n" << "EFI_BOOT" << ui->comboBox->currentText()+QVariant(DiskStruct[2].pNumb).toString());
                }
                else
                {
                    ui->textEdit->append("mkfs.fat -n EFI_BOOT -F32 " + ui->comboBox->currentText()+QVariant(DiskStruct[2].pNumb).toString()+"\n");
                    mainProcess->start("mkfs.fat", QStringList() << "-n" << "EFI_BOOT" << "-F32" << ui->comboBox->currentText()+QVariant(DiskStruct[2].pNumb).toString());
                }
                mainProcess->waitForStarted();
                procID++;
                pidIntl=mainProcess->pid();
                PidValueLabel->setText(QVariant(pidIntl).toString());
                ui->pbKill->setEnabled(true);
            }
            else
                {clearProcess();}
        }
        break;
        case 5:
        {
            if (exitStatus==QProcess::NormalExit)
            {
                blockCount++;
                if (blockCount<=maxCurPartition)
                {
                    switch (blockCount)
                    {
                        case 3:
                        {
                            QStringList fsOption;
                            if (ui->cbBootFS->currentText().toLower()!="vfat")
                            {
                                fsOption << "-L" << "Boot" << "-m" << "1" << ui->comboBox->currentText()+QVariant(DiskStruct[3].pNumb).toString();
                                ui->textEdit->append("mkfs."+ui->cbBootFS->currentText().toLower()+" -L Boot -m 1 " + ui->comboBox->currentText()+QVariant(DiskStruct[3].pNumb).toString()+"\n");
                            }
                            else
                            {
                                fsOption << "-n" << "BOOT" << ui->comboBox->currentText()+QVariant(DiskStruct[3].pNumb).toString();
                                ui->textEdit->append("mkfs."+ui->cbBootFS->currentText().toLower()+" -n BOOT " + ui->comboBox->currentText()+QVariant(DiskStruct[3].pNumb).toString()+"\n");
                            }
                            mainProcess->start("mkfs."+ui->cbBootFS->currentText().toLower(), fsOption );
                        }
                        break;
                        case 4:
                        {
                            if (!ui->checkBox->isChecked())
                            {
                                ui->textEdit->append("mkfs.ext4 -L Data -m 0.5 " + ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString()+"\n");
                                mainProcess->start("mkfs.ext4", QStringList() << "-L" << "Data" << "-m" << "0.5" << ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString());
                            }
                            else
                            {
                                ui->textEdit->append("mkfs.ext4 -n Data " + ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString()+"\n");
                                mainProcess->start("mkfs.ext4", QStringList() << "-n" << "Data" << ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString());
                            }
                        }
                        break;
                        default:
                        break;
                    }
                    mainProcess->waitForStarted();
                    pidIntl=mainProcess->pid();
                    PidValueLabel->setText(QVariant(pidIntl).toString());
                    ui->pbKill->setEnabled(true);
                }
                else
                    {clearProcess();}
            }
            else
                {clearProcess();}
        }
        break;
        case 6:
        {
            if (exitStatus==QProcess::NormalExit)
            {
                QString outStr;
                outStr=mountPath(0, false, "boot");
                if (!outStr.isEmpty())
                    {ui->textEdit->append(outStr);}
            }
            clearProcess();
        }
        break;
        case 7:
        {
            if (exitStatus==QProcess::NormalExit && exitCode==0)
            {
                QFile efiFile;
                efiFile.setFileName("/media/ubfcreator/efi/EFI/BOOT/BOOT"+*iArch+".efi");
                if (!efiFile.exists())
                {
                    bool ok;
                    QStringList listFileName;
                    listFileName << "boot" << "BOOT" << "grub" << "GRUB";
                    ok=false;
                    for (int i=0;i<listFileName.count();i++)
                    {
                        QStringList listFileLastName;
                        listFileLastName << "" << *iArch;
                        for (int j=0;j<listFileLastName.count();j++)
                        {
                            QStringList listFileSuffix;
                            listFileSuffix << "efi" << "EFI";
                            for (int k=0;k<listFileSuffix.count();k++)
                            {
                                QFile efiFileSearch;
                                efiFileSearch.setFileName("/media/ubfcreator/efi/EFI/BOOT/"+listFileName.at(i)+listFileLastName.at(j)+"."+listFileSuffix.at(k));
                                if (efiFile.exists())
                                {
                                    if (efiFile.fileName().toLower()!="/media/ubfcreator/efi/efi/boot/boot"+*iArch+".efi")
                                        {efiFileSearch.copy("/media/ubfcreator/efi/EFI/BOOT/BOOT"+*iArch+".efi");}
                                    ok=true;
                                    break;
                                }
                            }
                            if (ok)
                                {break;}
                        }
                        if (ok)
                            {break;}
                    }
                }
                if (efiFile.exists())
                {
                    createEfiConfig();
                    createGrubConfig();
                    clearProcess();
                }
                else
                    {extractEFIfile();}
            }
            else
                {extractGrubFolder();}
        }
        break;
        case 8:
        {
            if (exitStatus==QProcess::NormalExit)
            {
                extractEFIfile();
                createGrubConfig();
            }
            else
                {clearProcess();}
        }
        break;
        case 9:
        {
            if (exitStatus==QProcess::NormalExit)
                {createEfiConfig();}
            clearProcess();
        }
        break;
        default:
        break;
    }
}

void MainWindow::haseTimeOut()
{
    QProcess defProcess;
    defProcess.start("killall", QStringList() << "-USR1" << "dd");
    defProcess.waitForStarted();
    defProcess.waitForFinished();
}

void MainWindow::haseReadError()
{
    QString tmpStr;
    QString tempStr;
    long tmpInt;
    tmpStr=QVariant(mainProcess->readAllStandardError()).toString();
    if (procID>=1 && procID<=3)
    {
        if (!tmpStr.isEmpty())
        {
            if (tmpStr.split("\n",QString::SkipEmptyParts).count()>2)
            {
                tempStr=tmpStr.split("\n",QString::SkipEmptyParts).at(1).split("+", QString::SkipEmptyParts).first();
                tmpInt=tempStr.toLong();
                if (procID!=3)
                    {ui->progressBar->setValue(tmpInt);}
                else
                    {ui->progressBar->setValue(blockCount+tmpInt);}
                if (procID==1)
                    {qDebug() << "Clear blocks" << ui->progressBar->value();}
            }
        }
    }
    else
        {ui->textEdit->append(tmpStr);}
}

void MainWindow::haseReadStandart()
{
    QString tmpStr;
    QStringList tempStr;
    tmpStr=QVariant(mainProcess->readAllStandardOutput()).toString();
    if (procID!=4 || (procID==4 && gdiskCMD%5==0))
    {
        if (procID!=4)
        {
            ui->textEdit->append(tmpStr);
            ui->textEdit->append("\n");
        }
        else
        {
            if (gdiskCMD%5==0)
            {
                if (!tmpStr.isEmpty())
                {
                    tempStr=tmpStr.split("\n",QString::SkipEmptyParts);
                    tempStr.removeLast();
                    for (int i=0;i<tempStr.count();i++)
                        {ui->textEdit->append(tempStr.at(i));}
                }
            }
        }
    }
    switch (procID)
    {
        case 4:
        {
            switch (gdiskCMD%5)
            {
                case 0:
                {
                    curPartition++;
                    if (curPartition<=maxCurPartition)
                        {mainProcess->write(QVariant("n\n").toByteArray());}
                    else
                        {mainProcess->write(QVariant("w\n").toByteArray());}
                }
                break;
                case 1:
                {
                    if (curPartition<=maxCurPartition)
                    {
                        mainProcess->write(DiskStruct[curPartition].pNumb+QVariant("\n").toByteArray());
                        ui->textEdit->append("Create partition "+QVariant(DiskStruct[curPartition].pNumb).toString().remove("\n")+" size of "+DiskStruct[curPartition].sizeName+" and type of "+DiskStruct[curPartition].typeName);
                    }
                    else
                        {mainProcess->write(QVariant("y\n").toByteArray());}
                }
                break;
                case 2:
                {
                    if (curPartition<=maxCurPartition)
                        {mainProcess->write(QVariant("\n").toByteArray());}
                }
                break;
                case 3:
                {
                    if (curPartition<=maxCurPartition)
                        {mainProcess->write(DiskStruct[curPartition].pSize);}
                }
                break;
                case 4:
                {
                    if (curPartition<=maxCurPartition)
                        {mainProcess->write(DiskStruct[curPartition].pType);}
                }
                break;
                default:
                break;
            }
            gdiskCMD++;
        }
        break;
        default:
        break;
    }
}

void MainWindow::OpenAboutQt()
{
    QMessageBox::aboutQt(this);
}

void MainWindow::doExit()
{
    this->close();
}

void MainWindow::on_pbClear_clicked()
{
    createProcess(1);
    mainTimer = new QTimer(this);
    connect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
    ui->progressBar->setVisible(true);
    ui->progressBar->setEnabled(true);
    ui->progressBar->setMaximum(DiskInfo[ui->comboBox->currentText()].dSizeBlocks);
    ui->pbKill->setEnabled(true);
    mainTimer->setInterval(500);
    ui->textEdit->setText("dd if=/dev/zero of=" + ui->comboBox->currentText()+"\n");
    mainProcess->start("dd", QStringList() << "if=/dev/zero" << "of="+ui->comboBox->currentText());
    mainProcess->waitForStarted();
    pidIntl=mainProcess->pid();
    PidValueLabel->setText(QVariant(pidIntl).toString());
    mainTimer->start();
}

void MainWindow::on_pbrePlace_clicked()
{
    isWinComp();
    createProcess(4);
    blockCount=2;
    gdiskCMD=0;
    curPartition=0;
    ui->textEdit->setText("gdisk " + ui->comboBox->currentText()+"\n");
    mainProcess->start("gdisk", QStringList() << ui->comboBox->currentText());
    mainProcess->waitForStarted();
    pidIntl=mainProcess->pid();
    PidValueLabel->setText(QVariant(pidIntl).toString());
}

bool MainWindow::notMounted(QString diskName)
{
    QProcess defProcess;
    QString tmpStr;
    defProcess.start("mount");
    defProcess.waitForStarted();
    defProcess.waitForFinished();
    tmpStr=QVariant(defProcess.readAllStandardOutput()).toString();
    if (tmpStr.indexOf(diskName)==-1)
        {return(true);}
    else
        {return(false);}
}

bool MainWindow::isRemovable(QString diskName)
{
    QProcess defProcess;
    QString tmpStr;
    defProcess.start("dmesg");
    defProcess.waitForStarted();
    defProcess.waitForFinished();
    tmpStr=QVariant(defProcess.readAllStandardOutput()).toString();
    if (!tmpStr.isEmpty())
    {
        QStringList tmpStrL;
        bool Ok;
        Ok=false;
        tmpStrL=tmpStr.split("\n", QString::SkipEmptyParts);
        for (int i=0;i<tmpStrL.count();i++)
        {
            if (tmpStrL.at(i).indexOf(diskName)!=-1 && tmpStrL.at(i).indexOf("removable disk")!=-1)
                {Ok=true;}
        }
        return(Ok);
    }
    else
    {return(false);}
}

void MainWindow::isWinComp()
{
    DiskStruct.clear();
    if (!ui->checkBox->isChecked())
    {
        if (!ui->chbHaveWindowsUserData->isChecked() || DiskInfo[ui->comboBox->currentText()].dSizeBlocks<11739600)
        {
            DiskStruct[1].pNumb=QVariant("1").toByteArray();
            DiskStruct[1].pSize=QVariant("+1M\n").toByteArray();
            DiskStruct[1].pType=QVariant("ef02\n").toByteArray();
            DiskStruct[1].sizeName="1 MB";
            DiskStruct[1].typeName="BIOS boot partition";
            DiskStruct[2].pNumb=QVariant("2").toByteArray();
            if (!ui->chbFAT32->isChecked())
            {
                DiskStruct[2].pSize=QVariant("+8M\n").toByteArray();
                DiskStruct[2].sizeName="8 MB";
            }
            else
            {
                DiskStruct[2].pSize=QVariant("+34M\n").toByteArray();
                DiskStruct[2].sizeName="34 MB";
            }
            DiskStruct[2].pType=QVariant("ef00\n").toByteArray();
            DiskStruct[2].typeName="EFI System";
            DiskStruct[3].pNumb=QVariant("3").toByteArray();
            DiskStruct[3].pSize=QVariant("+34M\n").toByteArray();
            DiskStruct[3].sizeName="34 MB";
            if (ui->cbBootFS->currentText().toLower()!="vfat")
            {
                DiskStruct[3].pType=QVariant("8300\n").toByteArray();
                DiskStruct[3].typeName="Linux filesystem";
            }
            else
            {
                DiskStruct[3].pType=QVariant("0700\n").toByteArray();
                DiskStruct[3].typeName="MS Windows filesystem";
            }
            if (DiskInfo[ui->comboBox->currentText()].dSizeBlocks<11739600)
            {
                DiskStruct[4].pNumb=QVariant("4").toByteArray();
                DiskStruct[4].pSize=QVariant("\n").toByteArray();
                DiskStruct[4].pType=QVariant("8300\n").toByteArray();
                DiskStruct[4].sizeName="All free space";
                DiskStruct[4].typeName="Linux filesystem";
                maxCurPartition=4;
            }
            else
                {maxCurPartition=3;}
        }
        else
        {
            DiskStruct[1].pNumb=QVariant("2").toByteArray();
            DiskStruct[1].pSize=QVariant("+1M\n").toByteArray();
            DiskStruct[1].pType=QVariant("ef02\n").toByteArray();
            DiskStruct[1].sizeName="1 MB";
            DiskStruct[1].typeName="BIOS boot partition";
            DiskStruct[2].pNumb=QVariant("3").toByteArray();
            if (!ui->chbFAT32->isChecked())
            {
                DiskStruct[2].pSize=QVariant("+4M\n").toByteArray();
                DiskStruct[2].sizeName="4 MB";
            }
            else
            {
                DiskStruct[2].pSize=QVariant("+34M\n").toByteArray();
                DiskStruct[2].sizeName="34 MB";
            }
            DiskStruct[2].pType=QVariant("ef00\n").toByteArray();
            DiskStruct[2].typeName="EFI System";
            DiskStruct[3].pNumb=QVariant("4").toByteArray();
            if (!ui->chbFAT32->isChecked())
            {
                DiskStruct[3].pSize=QVariant("+16M\n").toByteArray();
                DiskStruct[3].sizeName="16 MB";
            }
            else
            {
                DiskStruct[3].pSize=QVariant("+34M\n").toByteArray();
                DiskStruct[3].sizeName="34 MB";
            }
            if (ui->cbBootFS->currentText().toLower()!="vfat")
            {
                DiskStruct[3].pType=QVariant("8300\n").toByteArray();
                DiskStruct[3].typeName="Linux filesystem";
            }
            else
            {
                DiskStruct[3].pType=QVariant("0700\n").toByteArray();
                DiskStruct[3].typeName="MS Windows filesystem";
            }
            maxCurPartition=3;
        }
    }
    else
    {
        DiskStruct[1].pNumb=QVariant("3").toByteArray();
        DiskStruct[1].pSize=QVariant("+1M\n").toByteArray();
        DiskStruct[1].pType=QVariant("ef02\n").toByteArray();
        DiskStruct[1].sizeName="1 MB";
        DiskStruct[1].typeName="BIOS boot partition";
        DiskStruct[2].pNumb=QVariant("2").toByteArray();
        if (!ui->chbFAT32->isChecked())
        {
            DiskStruct[2].pSize=QVariant("+4M\n").toByteArray();
            DiskStruct[2].sizeName="4 MB";
        }
        else
        {
            DiskStruct[2].pSize=QVariant("+34M\n").toByteArray();
            DiskStruct[2].sizeName="34 MB";
        }
        DiskStruct[2].pType=QVariant("ef00\n").toByteArray();
        DiskStruct[2].typeName="EFI System";
        DiskStruct[3].pNumb=QVariant("1").toByteArray();
        DiskStruct[3].pSize=QVariant("+16M\n").toByteArray();
        DiskStruct[3].pType=QVariant("0700\n").toByteArray();
        DiskStruct[3].sizeName="All free space";
        DiskStruct[3].typeName="MS Windows filesystem";
        maxCurPartition=3;
    }
}

void MainWindow::createProcess(int curID)
{
    mainProcess = new QProcess(this);
    connect(mainProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(proccessFinished(int,QProcess::ExitStatus)));
    connect(mainProcess, SIGNAL(readyReadStandardError()), this, SLOT(haseReadError()));
    connect(mainProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(haseReadStandart()));
    procID=curID;
}

void MainWindow::clearProcess()
{
    disconnect(mainProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(proccessFinished(int,QProcess::ExitStatus)));
    disconnect(mainProcess, SIGNAL(readyReadStandardError()), this, SLOT(haseReadError()));
    disconnect(mainProcess, SIGNAL(readyReadStandardOutput()), this, SLOT(haseReadStandart()));
    delete mainProcess;
    mainProcess=0;
    procID=0;
}

void MainWindow::disableButtons()
{
    ui->pbClear->setEnabled(false);
    ui->pbClearFast->setEnabled(false);
    ui->pbrePlace->setEnabled(false);
    ui->pbLegacy->setEnabled(false);
    ui->pbEFI->setEnabled(false);
}

QString MainWindow::getSharedPath()
{
    QString outStr;
    if (QCoreApplication::applicationDirPath()=="/usr/bin")
        {outStr="/usr/share/"+QCoreApplication::applicationName();}
    else
    {
        if (QCoreApplication::applicationDirPath()=="/usr/local/bin")
            {outStr="/usr/local/share/"+QCoreApplication::applicationName();}
        else
        {
            QDir dirP;
            dirP.setPath(QCoreApplication::applicationDirPath()+QDir::separator()+"files");
            if (dirP.exists())
                {outStr=QCoreApplication::applicationDirPath();}
            else
                {outStr=QCoreApplication::applicationDirPath()+"../UniversalBootFlashCreator";}
        }
    }
    return(outStr);
}

bool MainWindow::isMountPartition(QString inpPart, QString mountPoint)
{
    QProcess defProcess;
    QString tmpStr;
    defProcess.start("mount");
    defProcess.waitForStarted();
    defProcess.waitForFinished();
    tmpStr=QVariant(defProcess.readAllStandardOutput()).toString();
    return(tmpStr.indexOf(inpPart+" on "+mountPoint)!=-1);
}

QString MainWindow::mountPath(int diskID, bool mp, QString MountPath)
{
    QString outStr;
    QProcess defProcess;
    QDir bDir;
    bDir.setPath("/media/ubfcreator/"+MountPath);
    if (mp)
    {
        if (!bDir.exists())
        {
            bDir.mkpath(bDir.path());
            ui->textEdit->append("mount " + ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString()+" "+bDir.path()+"\n");
            defProcess.start("mount", QStringList() << ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString() << bDir.path());
            if (!isMountPartition(ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString(), bDir.path()))
                {outStr.append(ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString()+ " not mounted\n");}
        }
    }
    else
    {
        ui->textEdit->append("umount "+bDir.path());
        defProcess.start("umount", QStringList() << bDir.path());
    }
    defProcess.waitForStarted();
    defProcess.waitForFinished();
    if (!mp)
    {
        if (isMountPartition(ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString(), bDir.path()))
            {outStr.append(ui->comboBox->currentText()+QVariant(DiskStruct[diskID].pNumb).toString()+ " not mounted\n");}
        else
        {
            bDir.removeRecursively();
            bDir.rmdir(bDir.dirName());
        }
    }
    outStr=QVariant(defProcess.readAll()).toString();
    return(outStr);
}

QString MainWindow::readResConfFile(QString inpStr)
{
    QString outStr;
    QFile resFile;
    resFile.setFileName(inpStr);
    if (resFile.open(QIODevice::ReadOnly))
    {
        QByteArray ba;
        ba=resFile.readAll();
        outStr=QVariant(ba).toString();
        resFile.close();
    }
    return(outStr);
}

QString MainWindow::getUUID(QString devName)
{
    QString outStr;
    if (!devName.isEmpty())
    {
        QString strOut;
        QProcess defProcess;
        ui->textEdit->append("blkid " + devName);
        defProcess.start("blkid", QStringList() << devName);
        defProcess.waitForStarted();
        defProcess.waitForFinished();
        strOut=QVariant(defProcess.readAll()).toString();
        outStr=strOut.split(" UUID=\"",QString::SkipEmptyParts).last().split("\" ",QString::SkipEmptyParts).first();
    }
    return(outStr);
}

void MainWindow::createEfiConfig()
{
    QFile confFile;
    confFile.setFileName("/media/ubfcreator/efi/EFI/BOOT/grub.cfg");
    if (!confFile.exists())
    {
        QString confStr;
        confStr=readResConfFile(":/EFI/grub.cfg");
        if (!confStr.isEmpty())
        {
            QTextDocument *txtDoc;
            QTextDocumentWriter wrt("/media/ubfcreator/efi/EFI/BOOT/grub.cfg", "plaintext");
            confStr.replace("%%UUID_3%%", getUUID(ui->comboBox->currentText()+QVariant(DiskStruct[3].pNumb).toString())).replace("%%PART_3%%", QVariant(DiskStruct[3].pNumb).toString());
            txtDoc = new QTextDocument(confStr, this);
            wrt.write(txtDoc);
            txtDoc->deleteLater();
        }
    }
    QString outStr;
    outStr=mountPath(0, false, "efi");
    if (!outStr.isEmpty())
        {ui->textEdit->append(outStr);}
}

void MainWindow::createGrubConfig()
{
    if (DiskStruct.keys().contains(4))
    {
        QString confStr;
        confStr=readResConfFile(":/GRUB2/grubenv");
        if (!confStr.isEmpty())
        {
            QTextDocument *txtDoc;
            QTextDocumentWriter wrt("/media/ubfcreator/boot/grub2/grubenv", "plaintext");
            confStr.replace("%%UUID_4%%", getUUID(ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString()));
            txtDoc = new QTextDocument(confStr, this);
            wrt.write(txtDoc);
            txtDoc->deleteLater();
        }
        confStr.clear();
        confStr=readResConfFile(":/GRUB2/grub.cfg");
        if (!confStr.isEmpty())
        {
            QTextDocument *txtDoc;
            QTextDocumentWriter wrt("/media/ubfcreator/boot/grub2/grub.cfg", "plaintext");
            confStr.replace("%%UUID_4%%", getUUID(ui->comboBox->currentText()+QVariant(DiskStruct[4].pNumb).toString())).replace("%%PART_3%%", QVariant(DiskStruct[3].pNumb).toString()).replace("%%REBOOT%%", "Reboot").replace("%%PC_WILL_BE_REBOOT%%", "Reboot").replace("%%SHUTDOWN%%", "Shutdown").replace("%%SHUTDOWN_PC%%", "Shutdown");
            txtDoc = new QTextDocument(confStr, this);
            wrt.write(txtDoc);
            txtDoc->deleteLater();
        }
    }
    QString outStr;
    outStr=mountPath(0, false, "boot");
    if (!outStr.isEmpty())
        {ui->textEdit->append(outStr);}
}

void MainWindow::extractEFIfile()
{
    procID=9;
    mainProcess->setWorkingDirectory("/media/ubfcreator/efi");
    ui->textEdit->append("tar xJf "+getSharedPath()+"/files/EFI"+*iArch+".tar.xz\n");
    mainProcess->start("tar", QStringList() << "xJf" << getSharedPath()+"/files/EFI"+*iArch+".tar.xz");
    mainProcess->waitForStarted();
    pidIntl=mainProcess->pid();
    ui->pbKill->setEnabled(true);
}

void MainWindow::extractGrubFolder()
{
    procID=8;
    QString outStr;
    outStr=mountPath(3, true, "boot");
    if (outStr.isEmpty())
    {
        mainProcess->setWorkingDirectory("/media/ubfcreator/boot");
        qDebug () << "tar xJf "+getSharedPath()+"/files/GRUB2.tar.xz";
        ui->textEdit->append("tar xJf "+getSharedPath()+"/files/GRUB2.tar.xz");
        mainProcess->start("tar", QStringList() << "xJf" << getSharedPath()+"/files/GRUB2.tar.xz");
        mainProcess->waitForStarted();
        pidIntl=mainProcess->pid();
        ui->pbKill->setEnabled(true);
    }
    else
        {ui->textEdit->append(outStr);}
}

void MainWindow::on_pbRefreshList_clicked()
{
    mainProcess = new QProcess(this);

    ui->comboBox->clear();
    DiskInfo.clear();
    ui->label->clear();

    QStringList tmpStrList;
    mainProcess->start("fdisk", QStringList() << "-l");
    mainProcess->waitForStarted();
    mainProcess->waitForFinished();

    tmpStrList=QVariant(mainProcess->readAllStandardOutput()).toString().split("\n",QString::SkipEmptyParts);
    for (int i=0;i<tmpStrList.count();i++)
    {
        if ( tmpStrList.at(i).indexOf("/ram")==-1 && tmpStrList.at(i).indexOf("Disk /")==0 || tmpStrList.at(i).indexOf("Диск /")==0)
        {
            QString tmpStr;
            tmpStr=tmpStrList.at(i).split(" ",QString::SkipEmptyParts).at(1);
            if (notMounted(tmpStr.remove(':')) && isRemovable(tmpStr.split("/",QString::SkipEmptyParts).last()))
            {
                DiskInfo[tmpStr.remove(':')].dSize=tmpStrList.at(i).split(" ",QString::SkipEmptyParts).at(2)+" "+tmpStrList.at(i).split(" ",QString::SkipEmptyParts).at(3);
                DiskInfo[tmpStr.remove(':')].dSizeBytes=tmpStrList.at(i).split(" ",QString::SkipEmptyParts).at(4).toInt();
                DiskInfo[tmpStr.remove(':')].dSizeBlocks=tmpStrList.at(i).split(" ",QString::SkipEmptyParts).at(6).toInt();
            }
        }
    }
    ui->comboBox->addItems(DiskInfo.keys());

    delete mainProcess;
    mainProcess=0;
}

void MainWindow::on_comboBox_currentTextChanged(const QString &arg1)
{
    if (ui->comboBox->currentIndex()>=0)
    {
        ui->label->setText(DiskInfo[arg1].dSize);
        ui->pbClear->setEnabled(true);
        ui->pbClearFast->setEnabled(true);
        ui->pbrePlace->setEnabled(true);
        ui->pbLegacy->setEnabled(true);
        ui->pbEFI->setEnabled(true);
        if (DiskInfo[arg1].dSizeBlocks<11739600)
            {ui->chbHaveWindowsUserData->setEnabled(false);}
        else
            {ui->chbHaveWindowsUserData->setEnabled(true);}
    }
    else
        {disableButtons();}
}

void MainWindow::on_pbLegacy_clicked()
{
    QString outStr;
    isWinComp();
    outStr=mountPath(3, true, "boot");
    if (outStr.isEmpty())
    {
        createProcess(6);
        ui->textEdit->append("grub2-install --no-floppy --boot-directory=/media/ubfcreator/boot --target=i386-pc "+ui->comboBox->currentText());
        mainProcess->start("grub2-install", QStringList() << "--no-floppy" << "--boot-directory=/media/ubfcreator/boot" << "--target=i386-pc" << ui->comboBox->currentText());
        mainProcess->waitForStarted();
        pidIntl=mainProcess->pid();
        PidValueLabel->setText(QVariant(pidIntl).toString());
        ui->pbKill->setEnabled(true);
    }
    else
        {ui->textEdit->append(outStr);}
}

void MainWindow::on_pbEFI_clicked()
{
    QString outStr;
    isWinComp();
    outStr=mountPath(3, true, "boot");
    if (outStr.isEmpty())
        {outStr=mountPath(2, true, "efi");}
    if (outStr.isEmpty())
    {
        createProcess(7);
        iArch = new QString("x64");
        ui->textEdit->append("grub2-install  --removable --efi-directory=/media/ubfcreator/efi --boot-directory=/media/ubfcreator/boot --target=x86_64-efi\n");
        mainProcess->start("grub2-install", QStringList() << "--target=x86_64-efi" << "--removable" << "--efi-directory=/media/ubfcreator/efi" << "--boot-directory=/media/ubfcreator/boot");
        mainProcess->waitForStarted();
        pidIntl=mainProcess->pid();
        ui->pbKill->setEnabled(true);
    }
    else
        {ui->textEdit->append(outStr);}
}

void MainWindow::on_pbEFIx32_clicked()
{
    QString outStr;
    isWinComp();
    outStr=mountPath(3, true, "boot");
    if (outStr.isEmpty())
        {outStr=mountPath(2, true, "efi");}
    if (outStr.isEmpty())
    {
        createProcess(7);
        iArch = new QString("ia32");
        ui->textEdit->append("grub2-install  --removable --efi-directory=/media/ubfcreator/efi --boot-directory=/media/ubfcreator/boot --target=i386-efi\n");
        mainProcess->start("grub2-install", QStringList() << "--target=i386-efi" << "--removable" << "--efi-directory=/media/ubfcreator/efi" << "--boot-directory=/media/ubfcreator/boot");
        //ui->textEdit->append("tar xJf "+getSharedPath()+"/files/EFI.tar.xz");
        //mainProcess->start("tar", QStringList() << "xJf" << getSharedPath()+"/files/EFI.tar.xz");
        mainProcess->waitForStarted();
        pidIntl=mainProcess->pid();
        ui->pbKill->setEnabled(true);
    }
    else
        {ui->textEdit->append(outStr);}
}

void MainWindow::on_pbKill_clicked()
{
    mainTimer->stop();
    mainProcess->kill();
    ui->progressBar->setValue(0);
    pidIntl=0;
    PidValueLabel->clear();
    ui->pbKill->setEnabled(false);
    clearProcess();
    if (procID==1)
    {
        disconnect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
        delete mainTimer;
        mainTimer=0;
    }
    procID=0;
}

void MainWindow::on_pbClearFast_clicked()
{
    createProcess(2);
    blockCount=int(qPow(2,20))*32/512;
    mainTimer = new QTimer(this);
    connect(mainTimer, SIGNAL(timeout()), this, SLOT(haseTimeOut()));
    ui->progressBar->setVisible(true);
    ui->progressBar->setEnabled(true);
    ui->progressBar->setMaximum(blockCount+2048);
    ui->pbKill->setEnabled(true);
    mainTimer->setInterval(500);
    ui->textEdit->setText("dd if=/dev/zero of=" + ui->comboBox->currentText()+" count="+QVariant(blockCount).toString()+"\n");
    mainProcess->start("dd", QStringList() << "if=/dev/zero" << "of="+ui->comboBox->currentText() << "count="+QVariant(blockCount).toString());
    mainProcess->waitForStarted();
    pidIntl=mainProcess->pid();
    PidValueLabel->setText(QVariant(pidIntl).toString());
    mainTimer->start();
}

void MainWindow::on_checkBox_toggled(bool checked)
{
    if (checked)
    {
        ui->chbHaveWindowsUserData->setEnabled(false);
        ui->cbBootFS->setCurrentText("vFAT");
        ui->cbBootFS->setEnabled(false);
    }
    else
    {
        ui->cbBootFS->setEnabled(true);
        ui->cbBootFS->setCurrentText("EXT4");
        if (DiskInfo[ui->comboBox->currentText()].dSizeBlocks<11739600)
            {ui->chbHaveWindowsUserData->setEnabled(false);}
        else
            {ui->chbHaveWindowsUserData->setEnabled(true);}
    }
}

